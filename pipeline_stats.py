# pylint: disable=invalid-name
import sys
import csv
import gitlab  # type: ignore
from gitlab import Gitlab
from gitlab.v4.objects import Project, ProjectJob, ProjectPipeline  # type: ignore
import json
import os

HEADER = {
    'pipeline_id': "Pipeline id",
    'id': "Job id",
    'stage': 'Stage',
    'name': 'Name',
    'started_at': 'Started at',
    'duration': 'Duration',
    'finished_at': 'Finished at',
    'status': 'Status',
}

FIELD_SEP = ';'

CSV_FORMAT = {
    'delimiter': FIELD_SEP,
    'quotechar': '\\',
    'quoting': csv.QUOTE_MINIMAL,
    'fieldnames': HEADER.keys(),
}


def init():
    gl = gitlab.Gitlab.from_config('gitlab', ['gl.cfg'])
    gl.auth()
    return gl


def eprint(*args, **kwargs) -> None:
    print(*args, file=sys.stderr, **kwargs)


def usage(args):
    print(f"Usage: {args[0]} <gitlab-project-id> <gitlab-pipeline-id>")
    print("")
    print(f"E.g.: {args[0]} 9487506 216272777")
    print("")
    print("Project IDs of interest: ")
    print(" nomadiclabs/tezos: 9487506")
    print(" metastatedev/tezos: 11412215")
    print("Requires a gl.cfg file (see gl.cfg.MOVEME)")


def pipeline_from_files(p: Project, pl_iid: int):
    pl_attrs = json.load(open(f'_data/pipeline-{pl_iid}.json'))
    pl = ProjectPipeline(p.pipelines, pl_attrs)

    jobs = [
        ProjectJob(pl.jobs, json.load(open(f'_data/{job_json_file}')))
        for job_json_file in os.listdir('_data')
        if job_json_file.startswith(f'pipeline-{pl_iid}-job-')
    ]

    eprint(f"Read pipeline {p.id}/{pl_iid} and {len(jobs)} jobs")

    return (pl, jobs)


def pipeline_from_api(gl: Gitlab, project_id: int, pl_iid: int):
    p = gl.projects.get(project_id, lazy=True)
    pl = p.pipelines.get(pl_iid)
    assert (
        pl.status != 'running'
    ), 'The script only works on completed pipeline'
    jobs = pl.jobs.list(all=True)
    return (pl, jobs)


def save_pipeline(pl, jobs) -> None:
    pl_path = f'_data/pipeline-{pl.id}.json'
    with open(pl_path, 'w') as pl_output:
        pl_output.write(json.dumps(pl.attributes))
        eprint(f"wrote {pl_path}")

    for j in jobs:
        job_path = f'_data/pipeline-{pl.id}-job-{j.id}.json'
        with open(job_path, 'w') as job_output:
            job_output.write(json.dumps(j.attributes))
            eprint(f"wrote {job_path}")


if __name__ == '__main__':
    gl = init()
    if len(sys.argv) != 3:
        usage(sys.argv)
        exit(1)

    project_id = int(sys.argv[1])
    pl_iid = int(sys.argv[2])
    p = gl.projects.get(project_id, lazy=True)

    pl = p.pipelines.get(pl_iid)
    (pl, jobs) = pipeline_from_api(gl, project_id, pl_iid)
    save_pipeline(pl, jobs)

    # (pl, jobs) = pipeline_from_files(p, pl_iid)

    all_pseudo_job = {k: getattr(pl, k, None) for (k, _) in HEADER.items()}
    all_pseudo_job['pipeline_id'] = pl_iid
    all_pseudo_job['id'] = '-1'
    all_pseudo_job['stage'] = 'All'
    all_pseudo_job['name'] = 'All'

    def job_to_row(j: ProjectJob):
        row = {k: getattr(j, k) for (k, _) in HEADER.items() if hasattr(j, k)}
        return row

    rows = [HEADER, all_pseudo_job] + list(map(job_to_row, jobs))

    csvwriter = csv.DictWriter(sys.stdout, **CSV_FORMAT)  # type: ignore
    csvwriter.writerows(rows)
