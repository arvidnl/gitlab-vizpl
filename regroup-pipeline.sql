SELECT
    Stage,
    Stage as Name,
    `Started at`,
    `Duration`,
    `Finished at`,
    Status,
    (CASE Status
      WHEN 1 THEN '0x89c9b8' -- color_success
      WHEN 2 THEN '0xd9adad' -- color_failed
      WHEN 0 THEN '0xdddddd' -- color_manual
      ELSE '0x0a043c' -- color_other
    END) as `__color`
FROM (SELECT
    Stage, Name,
    MIN(`Started at`) as 'Started at',
    MAX(`Duration`) as 'Duration',
    MAX(`Finished at`) as 'Finished at',
    MAX(CASE Status
          WHEN 'success' THEN 1
          WHEN 'failed' THEN 2
          WHEN 'manual' THEN 0
          ELSE 99
     END) as `Status`
    FROM jobs
    GROUP BY stage)
ORDER BY `Started at` DESC, `Duration`

