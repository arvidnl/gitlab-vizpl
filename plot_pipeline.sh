#!/bin/bash

set -e
set -x

if [[ -z "$1" ]]; then
    echo "Usage: $0 <pipeline-data.csv> [--sort|--group]"
    echo ""
    echo "If --sort is given, jobs are grouped by starting minute, and "
    echo "ordered by duration in that group."
    echo "If --group is given, one line per stage is produced"
    exit 1
fi

pipeline="$1"

input_data=$1
output_plot=xdg${input_data/.csv/.png}

input_data_transformed=`tempfile`.csv
if [[ "$2" = "--sort" ]]; then
    query="$(<sort-pipeline.sql)"
elif [[ "$2" = "--group" ]]; then
    query="$(<regroup-pipeline.sql)"
else
    query="$(<nogroup-pipeline.sql)"
fi

csvsql --tables jobs -I -d';' --query "$query" < $input_data | csvformat -D';' > $input_data_transformed
cat $input_data_transformed | head | csvlook

y=$(wc -l $input_data_transformed | cut -f1 -d' ')
wx=1500
wy=$(echo "300 + $y * 20" | bc)

gnuplot -e "pipeline='$pipeline';" \
        -e "input_data='$input_data_transformed';" \
        -e "set terminal png size $wx,$wy;" \
        -e "set output '$output_plot'" \
        -p pl_gannt.gnu
echo "Output in $output_plot"

xdg-open $output_plot
