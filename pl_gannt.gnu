set xdata time
timeformat = "%Y-%m-%dT%H:%M:%S"
set format x "%H:%M:%S"

set yrange [-1:]
XInterval = strptime("%M","10")
set xtics XInterval nomirror
set xtics scale 2, 0.5
set mxtics 4
set ytics nomirror
set grid x y
unset key
set title sprintf("Gantt For Pipeline %s\n\nJob start and end times", pipeline)
set border 3

T(N) = timecolumn(N,timeformat)

set datafile separator ';'

set termoption noenhanced


set style arrow 1 filled nohead size screen 0.01, 15 fixed lt 3 lw 4 lc rgb variable

# column indices, 1-indexed
stage=1
name=2
started_at=3
duration=4
finished_at=5
status=6
mycolor=7

set label font ", 20"

plot input_data skip 1 using (T(started_at)) : ($0) : (T(finished_at)-T(started_at)) : (0.0) : mycolor : yticlabel(2) with vectors as 1, \
     input_data skip 1 using (T(started_at)) : ($0) : (sprintf("%3d:%02d", column(duration)/60, int(column(duration))%60)) with labels right offset -2, 0.5  font ", 10"
