#!/bin/bash

set -e

if [[ -z "$1" || -z "$2" ]]; then
    echo "Usage: $0 <gitlab-project-id> <gitlab-pipeline-id> [--sort|--group]"
    echo ""
    echo "gitlab-project-ids:"
    echo " tezos/tezos: 3836952"
    echo " nomadiclabs/tezos: 9487506"
    echo " metastatedev/tezos: 11412215"

    exit 1
fi

poetry run python pipeline_stats.py $1 $2 > pl-$1-$2.csv
./plot_pipeline.sh pl-$1-$2.csv $3
