
# What this is

Downloads and plots a gitlab pipeline. Example:

![grouped](examples/pl-9487506-280663762-group.png)

# Install

The code is a savory mix of bash, python, sql and gnuplot. This is
data science.

Uses poetry for dependency management:

     poetry install

And you should be good.

# Usage

    $ ./fetch_plot_pipeline.sh 
    Usage: ./fetch_plot_pipeline.sh <gitlab-project-id> <gitlab-pipeline-id> [--sort|--group]

Example:

    $ ./fetch_plot_pipeline.sh 
    Usage: ./fetch_plot_pipeline.sh <gitlab-project-id> <gitlab-pipeline-id> [--sort|--group]

For instance, to graph the pipeline of
https://gitlab.com/nomadic-labs/tezos/-/pipelines/280663762.

    ./fetch_plot_pipeline.sh 9487506 280663762 --sort

## Gitlab Project IDs

Because it's annoying to always lookup project IDs:

 - tezos/tezos: 3836952
 - nomadiclabs/tezos: 9487506

# How it looks

## Outputs

### Regular

![vanilla](examples/pl-9487506-280663762.png)

### Sorted

![sorted](examples/pl-9487506-280663762-sorted.png)

### Grouped

![grouped](examples/pl-9487506-280663762-group.png)
