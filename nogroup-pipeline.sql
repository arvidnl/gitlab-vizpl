SELECT
    Stage,
    Name,
    `Started at`,
    `Duration`,
    `Finished at`,
    Status,
    (CASE Status
      WHEN 'success' THEN '0x89c9b8' -- color_success
      WHEN 'failed' THEN '0xd9adad' -- color_failed
      WHEN 'manual' THEN '0xdddddd' -- color_manual
      ELSE '0x0a043c' -- color_other
    END) as `__color`
FROM jobs ORDER BY `Started at` DESC, `Duration` DESC


