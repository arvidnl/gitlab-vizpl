SELECT
    -- name, duration,
    Stage,
    Name,
    `Started at`,
    `Duration`,
    `Finished at`,
    Status,
    (CASE Status
      WHEN 'success' THEN '0x89c9b8' -- color_success
      WHEN 'failed' THEN '0xd9adad' -- color_failed
      WHEN 'manual' THEN '0xdddddd' -- color_manual
      ELSE '0x0a043c' -- color_other
    END) as `__color`
-- FROM `pl-22795010-224727863`
FROM `jobs`
WHERE `Started at` IS NOT NULL
ORDER BY (CASE stage
          when 'All' then 0
          when 'sanity'then 1
          when 'build'then 2
          when 'test'then 3
          when 'doc'then 4
          when 'packaging'then 5
          when 'publish_coverage'then 6
          when 'test_coverage'then 7
          else 99
          END) DESC, CAST(Duration AS INT) ASC;

